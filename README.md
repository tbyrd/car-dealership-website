# CarCar

Team:

* Tyler - Sales
* Zac - Services

## Design
In our design we added a Customer Database to Inventory, because Services needs the customer name, but sales needs customer name, phone number and address.

Our JSON for "Create Customer":
    {
        "name": "Jim",
        "address": "1111 Main Street",
        "phone_number": "800 800 8888",
    }

## Service microservice

Explain your models and integration with the inventory microservice, here.
Technician:
    Tech Name and employee number need to be tracked

The JSON for creating a technician:
    {
	    "tech_name": "John Simms",
	    "employee_number": "JS1"
    }

Appointments:
    Service Appointment:
        VIN of the vehicle: need to get id in order to find VIN
        
        The name of the person to whom the vehicle belongs: need to get the id of the customer
        
        The date and time of the appointment: four digit year dash two digit month dash two digit day space two digit hour colon two digit minutes
        example: 2022-12-15 10:00
        
        The assigned technician: need the id of the technician in oredr to get all info
        
        A reason for the service appointment: text field limit 200 characters

    List of appointments:
        show a list of scheduled appointments that contain the details collected in the form: VIN, customer name, date and time of the appointment, the assigned technician's name, and the reason for the service.

    Service History:
        You need to show a list of the service appointments for a specific VIN. To do this, create a page that has an input that allows someone to type in the VIN. On form submission, fetch all of the service appointments for an automobile with the VIN in the input. Then, show that list of service appointments to include the customer name, date and time of the appointment, the assigned technician's name, and the reason for the service.

        You need to create a link in the navbar to get to the page that shows the service history for a specific VIN.

## Sales microservice

Explain your models and integration with the inventory microservice, here.

For my project, I created four models, a AutomobileVO, a CustomerVO, a SalesPerson and a SalesRecord model.  The Automobile VO and the CustomerVO are value objects that are connected to the Automobile and Customer models in the inventory microservice.  

The AutomobileVO Model:
The AutomobileVO has a vin property, which is a charfield with a max length of 17.  I chose to use a charfield for the vin number because vin numbers can include both numbers and characters.  Polling is used to connect the AutomobileVO with the inventory microservice.  

The CustomerVO Model:
The CustomerVO is a value object to the customer model in the inventory project.  As a team, we chose to include the Customer model in the Inventory microservices, instead of the Sales microservice because both the Sales microservice and the Service microservice need access to the customer data.  Polling is used to connect the CustomerVO with the inventory microservice.  

The SalesPerson Model:
The SalesPerson model includes a name and an employee number field.  The model is used in the "Add a Sales Person" form to gather the data about the sales people.  The model is also integrated into the "Sales Person History List," which shows the list of sales sorted by a specific sales person.  

The SalesRecord Model:
The Sales Record model includes a sales price, automobile, sales person and a customer field.  The sales price field is used in the Sales Record Form to record the sale price of a vehicle.  It is also used in the Sales List to display the sale price of an automobile.  The automobile field is a foreign key to the AutomobileVO, allowing me to access the vin number and id number for a specific automobile.  The sales person field is a foreign key to the SalesPerson model, it is used in the Sales Person form to record the sales person associated with the sale.  The customer field is a foreign key to the CustomerVO, it is used in the "Submit a Customer" form and the "Record a New Sale" form and the "Sales Person History List."