import React from 'react';

class SalesPersonHistoryList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sales: [],
            salesPeople: [],
            salesPersonId: ""
        };
        this.handleChangeSalesPerson = this.handleChangeSalesPerson.bind(this);
    }


    async componentDidMount() {
        const salesPeopleUrl = 'http://localhost:8090/api/employees/';
        const salesUrl = 'http://localhost:8090/api/sales/';

        const salesPeopleResponse = await fetch(salesPeopleUrl);
        const salesResponse = await fetch(salesUrl);

        if (salesPeopleResponse.ok && salesResponse.ok) {
            const salesPeopleData = await salesPeopleResponse.json();
            const salesData = await salesResponse.json();

            const stateAfterRequests = { salesPeople: salesPeopleData.employees, sales: salesData.sales }

            this.setState(stateAfterRequests);
        }
    }

    handleChangeSalesPerson(event) {
        const value = event.target.value;
        this.setState({ salesPersonId: value });
    }

    render() {
        return (
            <div className="row">
                <div>
                    <h1>Sales person history</h1>
                </div>
                <div className="offset-0 col-12">
                    <form id="create-salesperson-form">
                        <div className="mb-3">
                            <select onChange={this.handleChangeSalesPerson} value={this.state.salesPersonId} required name="sales_person" id="sales_person" className="form-select">
                                <option value="">Choose a sales person</option>
                                {this.state.salesPeople.map(employee => {
                                    return (
                                        <option key={employee.id} value={employee.id}>{employee.name}</option>
                                    )
                                })}
                            </select>
                        </div>
                    </form>
                </div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Sales Person</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Sale Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <React.Fragment>
                            {this.state.sales.filter(sale => sale.sales_person.id == this.state.salesPersonId).map(sale => {
                                return (
                                    <tr key={sale.href}>
                                        <td>{sale.sales_person.name}</td>
                                        <td>{sale.customer.name}</td>
                                        <td>{sale.automobile.vin}</td>
                                        <td>{sale.sales_price}</td>
                                    </tr>)
                            })}
                        </React.Fragment>
                    </tbody>
                </table>
            </div >
        );
    }
}

export default SalesPersonHistoryList; 
