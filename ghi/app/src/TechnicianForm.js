import React from "react";

class TechnicianForm extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            tech_name: '',
            employee_number: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        
        const techUrl = 'http://localhost:8080/technicians/'

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(techUrl, fetchConfig);
        if (response.ok) {
            const cleared = {
                tech_name: '',
                employee_number: '',
            };
            this.setState(cleared);
        } else {
            console.error("handleSubmit is not working correctly")
        }
    }

    handleChange(event) {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({[name]:value});
    }
    
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1 className="text-center">Enter a Technician</h1>
                        <form onSubmit={this.handleSubmit} id="create-technician-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} placeholder="Technician" required type="text" name="tech_name" id="id" className="form-control" value={this.state.tech_name}/>
                                <label htmlFor="tech_name">Name of Technician</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} placeholder="Employee_Number" required type="text" name="employee_number" id="id" className="form-control" value={this.state.employee_number}/>
                                <label htmlFor="employee_number">Employee Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default TechnicianForm;