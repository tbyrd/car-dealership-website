import React from "react";
import { useState, useEffect } from 'react';

function AppointmentsList() {
    const [appointments, setAppointments] = useState([]);

    const fetchAppointments = async () => {
        const url = 'http://localhost:8080/appointments/';
        const result = await fetch(url);
        const appointmentsJSON = await result.json();
        setAppointments(appointmentsJSON.appointments)
    }

    const deletion = async (id) => {
        const resp = await fetch(`http://localhost:8080/appointments/${id}`, {method: "DELETE"});
        const data = await resp.json();
        fetchAppointments();
    };

    const update = async (id) => {
        const updateResp = await fetch(`http://localhost:8080/appointments/${id}/`, {
            method: "PUT",
            body: JSON.stringify({
                finished: true
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        });
        const data = await updateResp.json();
        fetchAppointments();
    }

    useEffect(() => {
        fetchAppointments();
    }, []);

    return (
        <div>
            <h2>List of Unfinished Appointments</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {appointments?.map(appointment => {
                        let hours = (new Date(appointment.date_time).getUTCHours());
                        let minutes = (new Date(appointment.date_time).getUTCMinutes()<10?'0':'')+(new Date(appointment.date_time).getUTCMinutes())
                        let mid='am';
                        if(hours==0){
                            hours=12;
                        } else if(hours>12) {
                            hours=hours%12;
                            mid='pm'
                        }
                        if (appointment.finished === false) {
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.automobile.import_vin}</td>
                                    <td>{appointment.customer.customer_name}</td>
                                    <td>{new Date(appointment.date_time).getDate()+'/'+(1+(new Date(appointment.date_time).getMonth()))+'/'+new Date(appointment.date_time).getUTCFullYear()}</td>
                                    <td>{hours +':'+ minutes +' '+ mid}</td>
                                    <td>{appointment.technician.tech_name}</td>
                                    <td>{appointment.reason}</td>
                                    <td>
                                        <button onClick={()=> deletion(appointment.id)} className="btn btn-danger">Cancel</button>
                                    </td>
                                    <td>
                                    <button onClick={()=> update(appointment.id)} className="btn btn-success">Finished</button>
                                    </td>
                                </tr>
                            )
                        }
                    })}
                </tbody>
            </table>
        </div>
    )


}
export default AppointmentsList;