import { NavLink } from 'react-router-dom';
function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>

            <div className="dropdown ">
              <button className="btn btn-success dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false"> Inventory
              </button>
              <ul className='dropdown-menu dropdown-menu-dark'>
                <li className="nav-item">
                  <NavLink className="dropdown-item nav-link " to="/manufacturers/list/">Manufacturers</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item nav-link " to="/manufacturers/manufacturerform">Add a Manufacturer</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item nav-link " to="/vehicles/list/">Models</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item nav-link " to="/vehicles/vehiclemodelform">Add a Model</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item nav-link " to="/automobiles/list/">Automobiles</NavLink>
                </li>
                <li>
                  <NavLink className="dropdown-item nav-link " to="/automobiles/create/">Make Automobiles</NavLink>
                </li>
              </ul>
            </div>
            <div className='dropdown'>
              <button className="btn btn-success dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false"> Services
              </button>
              <ul className='dropdown-menu dropdown-menu-dark'>
                <li className="nav-item">
                  <NavLink className="dropdown-item nav-link" to="/appointments/list/">Appointments</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item nav-link" to="/appointments/create/">Make Appointment</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item nav-link" to="/appointments/history/">Service History</NavLink>
                </li>
              </ul>
            </div>

            <div className="dropdown">
            <button className="btn btn-success dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false"> Sales
              </button>
              <ul className="dropdown-menu dropdown-menu-dark">
                <li className="nav-item">
                  <NavLink className="dropdown-item nav-link " to="/salespersonform/">Add a Sales Person</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item nav-link " to="/salesrecordform">Create a Sales Record</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item nav-link " to="/saleslist">Sales</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item nav-link " to="/salespersonhistorylist">Sales Person History</NavLink>
                </li>
              </ul>
            </div>
            

            <li className="nav-item">
              <NavLink className="nav-link active" to="/customerform/">Add a Potential Customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" to="/technicians/create/">Add a Technician</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
