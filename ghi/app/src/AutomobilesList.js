import React from "react";
import { useState, useEffect } from "react";

function AutomobilesList() {
    const [autos, setAutomobiles] = useState([]);

    const fetchAutomobiles = async () => {
        const url = 'http://localhost:8100/api/automobiles/'
        const result = await fetch(url);
        const autosJSON = await result.json();
        setAutomobiles(autosJSON.autos);
    }

    useEffect(() => {
        fetchAutomobiles();
    }, []);


    return (
        <div>
            <h2>Automobiles</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {autos?.map(automobile => {
                        return (
                            <tr key={automobile.id}>
                                <td>{automobile.vin}</td>
                                <td>{automobile.color}</td>
                                <td>{automobile.year}</td>
                                <td>{automobile.model.name}</td>
                                <td>{automobile.model.manufacturer.name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )


}
export default AutomobilesList;