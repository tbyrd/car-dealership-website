import React from 'react';
import { useState, useEffect } from 'react';


function SalesList() {
    const [sales, setSales] = useState([]);

    const fetchSales = async () => {
        const url = 'http://localhost:8090/api/sales/';
        const result = await fetch(url);
        const salesJSON = await result.json();
        setSales(salesJSON.sales);
    }

    useEffect(() => {
        fetchSales();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Sales Person</th>
                    <th>Employee Number</th>
                    <th>Purchaser's Name</th>
                    <th>VIN Number</th>
                    <th>Sale Price</th>
                </tr>
            </thead>
            <tbody>
                {sales?.map(sale => {
                    return (
                        <tr key={sale.id}>
                            <td>{sale.sales_person.name}</td>
                            <td>{sale.sales_person.employee_number}</td>
                            <td>{sale.customer.name}</td>
                            <td>{sale.automobile.vin}</td>
                            <td>{sale.sales_price}</td>
                        </tr>)
                })}
            </tbody>
        </table >
    );
}

export default SalesList;