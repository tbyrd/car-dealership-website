import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React from "react";
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import VehicleModelsList from './VehicleModelsList';
import VehicleModelForm from './VehicleModelForm';
import AutomobilesList from './AutomobilesList';
import AutomobilesForm from './AutomobilesForm';

import SalesPersonForm from './SalesPersonForm';
import CustomerForm from './CustomerForm';
import SalesList from './SalesList';
import SalesPersonHistoryList from './SalesPersonHistoryList';
import SalesRecordForm from './SalesRecordForm';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import ServiceHistory from './ServiceHistory';

import AppointmentsList from './AppointmentsList';



function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers">
            <Route path="list" element={<ManufacturersList manufacturers={props.manufacturers} />} />
            <Route path="manufacturerform" element={<ManufacturerForm />} />
          </Route>
          <Route path="vehicles">
            <Route path="list" element={<VehicleModelsList vehicles={props.vehicles} />} />
            <Route path="vehiclemodelform" element={<VehicleModelForm />} />
          </Route>
          <Route path="automobiles">
            <Route path="list" element={<AutomobilesList automobiles={props.automobiles} />} />
            <Route path="create" element={<AutomobilesForm />} />
          </Route>
          <Route path="technicians">
            <Route path="create" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments">
            <Route path="list" element={<AppointmentsList appointments={props.appointments} />} />
            <Route path="create" element={<AppointmentForm />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>

          <Route path="/salespersonform" element={<SalesPersonForm />} />
          <Route path="/customerform" element={<CustomerForm />} />
          <Route path="/saleslist" element={<SalesList />} />
          <Route path="/salespersonhistorylist" element={<SalesPersonHistoryList />} />
          <Route path="/salesrecordform" element={<SalesRecordForm />} />

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
