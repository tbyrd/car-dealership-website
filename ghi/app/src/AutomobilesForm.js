import React from "react";

class AutomobilesForm extends React.Component {

    /* color, year, vin, model_id */
    constructor(props){
        super(props);
        this.state = {
            color: "",
            year: "",
            vin: "",
            models: [],
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };

        delete data.models;

        const autosUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(autosUrl, fetchConfig);
        if (response.ok) {
            const cleared = {
                color: "",
                year: "",
                vin: "",
                model_id: "",
            };
            this.setState(cleared);
        }
    }

    
    handleChange(event) {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({[name]:value});
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/models/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ models: data.models });
        }
    }

    render() {
        return (
            <div>
                <h2>Automobiles Form</h2>
                <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1 className="text-center">Make an Automobile</h1>
                            <form onSubmit={this.handleSubmit} id="create-automobile-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.color} placeholder="Automobile Color" required type="text" name="color" id="color" className="form-control" />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.year} placeholder="Automobile Year" required type="text" name="year" id="year" className="form-control" />
                                <label htmlFor="year">Year</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.vin} placeholder="Automobile VIN" required type="text" name="vin" id="vin" className="form-control" />
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChange} value={this.state.model_id} required name="model_id" id="model_id" className="form-select">
                                    <option value="">Choose a Model</option>
                                    {this.state.models.map(model => {
                                        return (
                                            <option key={model.id} value={model.id}>{model.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Submit</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}
export default AutomobilesForm;