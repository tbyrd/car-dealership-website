import React from 'react';

class SalesRecordForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            salesPrice: '',
            salesPeople: [],
            automobiles: [],
            customers: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeAutomobile = this.handleChangeAutomobile.bind(this);
        this.handleChangeSalesPerson = this.handleChangeSalesPerson.bind(this);
        this.handleChangeCustomer = this.handleChangeCustomer.bind(this);
        this.handleChangeSalesPrice = this.handleChangeSalesPrice.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.sales_person = data.salesPerson;
        data.sales_price = data.salesPrice;
        delete data.salesPerson;
        delete data.salesPrice;
        delete data.automobiles;
        delete data.salesPeople;
        delete data.customers;


        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            const newSale = await response.json();

            const cleared = {
                automobile: '',
                salesPerson: '',
                customer: '',
                salesPrice: '',
            };
            this.setState(cleared);
        }
    }

    handleChangeAutomobile(event) {
        const value = event.target.value;
        this.setState({ automobile: value });
    }

    handleChangeSalesPerson(event) {
        const value = event.target.value;
        this.setState({ salesPerson: value });
    }

    handleChangeCustomer(event) {
        const value = event.target.value;
        this.setState({ customer: value });
    }

    handleChangeSalesPrice(event) {
        const value = event.target.value;
        this.setState({ salesPrice: value });
    }


    async componentDidMount() {
        const salesPersonUrl = 'http://localhost:8090/api/employees/';
        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const customerUrl = 'http://localhost:8100/api/customers/';

        const responseSalesperson = await fetch(salesPersonUrl);
        const responseAutomobile = await fetch(automobileUrl);
        const responseCustomer = await fetch(customerUrl);

        if (responseSalesperson.ok && responseAutomobile.ok && responseCustomer.ok) {
            const salesPersonData = await responseSalesperson.json();
            const automobileData = await responseAutomobile.json();
            const customerData = await responseCustomer.json();
            this.setState({ salesPeople: salesPersonData.employees, automobiles: automobileData.autos, customers: customerData.customers });
        }

    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Record A New Sale</h1>
                        <form onSubmit={this.handleSubmit} id="create-sales-record-form">
                            <div className="mb-3">
                                <select onChange={this.handleChangeAutomobile} value={this.state.automobile} required name="automobile" id="automobile" className="form-select">
                                    <option value="">Choose an Automobile</option>
                                    {this.state.automobiles.map(automobile => {
                                        return (
                                            <option key={automobile.id} value={automobile.id}>{automobile.vin}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChangeCustomer} value={this.state.customer} required name="customer" id="customer" className="form-select">
                                    <option value="">Choose a Customer</option>
                                    {this.state.customers.map(customer => {
                                        return (
                                            <option key={customer.id} value={customer.id}>{customer.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChangeSalesPerson} value={this.state.salesPerson} required name="sales_person" id="sales_person" className="form-select">
                                    <option value="">Choose an Sales Person</option>
                                    {this.state.salesPeople.map(salesPerson => {
                                        return (
                                            <option key={salesPerson.id} value={salesPerson.id}>{salesPerson.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChangeSalesPrice} value={this.state.salesPrice} placeholder="Sales Price" required type="number" name="sales_price" id="sales_price" className="form-control" />
                                <label htmlFor="name">Sale Price</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div >
            </div >
        );
    }
}

export default SalesRecordForm;