import React, { useState, useEffect } from "react";

export default function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [filterValue, setFilterValue] = useState("");
    const [filterField, setFilterField] = useState("import_vin")

    const fetchAppointments = async () => {
        const url = 'http://localhost:8080/appointments/';
        const result = await fetch(url);
        const appointmentsJSON = await result.json();
        setAppointments(appointmentsJSON.appointments)
    }

    const getFilteredResults = () => {
        if (filterValue === "") {
            return appointments;
        }
        else if (filterField === "import_vin"){
            return appointments?.filter((appointment) => appointment["automobile"]["import_vin"]?.includes(filterValue))
        }
        else if (filterField === "customer_name"){
            return appointments?.filter((appointment) => appointment["customer"]["customer_name"]?.includes(filterValue))
        }
        else if (filterField === "tech_name"){
            return appointments?.filter((appointment) => appointment["technician"]["tech_name"]?.includes(filterValue))
        }
        else {
            return appointments?.filter((appointment) => appointment[filterField]?.includes(filterValue))
        }
               
    }

    useEffect(() => {
        fetchAppointments();
    }, []);

    const handleValue = (e) => {
        setFilterValue(e.target.value);

    };
    
    const handleFieldChange = (e) => {
        setFilterField(e.target.value);
    }

    return (
        <div>
            <h2>Service History</h2>
            <select onChange={handleFieldChange}>
                <option value="import_vin">VIN</option>
                <option value="customer_name">Customer</option>
                <option value="date_time">Date</option>
                <option value="date_time">Time</option>
                <option value="tech_name">Technician</option>
                <option value="reason">Reason</option>
            </select>
            <input value={filterValue} onChange={handleValue} />
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {getFilteredResults().map((appointment) => {
                        let hours = (new Date(appointment.date_time).getUTCHours());
                        let minutes = (new Date(appointment.date_time).getUTCMinutes()<10?'0':'')+(new Date(appointment.date_time).getUTCMinutes())
                        let mid='am';
                        if(hours==0){
                            hours=12;
                        } else if(hours>12) {
                            hours=hours%12;
                            mid='pm'
                        }
                        if (appointment.finished === true){
                            return (
                                
                                <tr key={appointment.id}>
                                    <td>{appointment.automobile.import_vin}</td>
                                    <td>{appointment.customer.customer_name}</td>
                                    <td>{new Date(appointment.date_time).getDate()+'/'+(1+(new Date(appointment.date_time).getMonth()))+'/'+new Date(appointment.date_time).getUTCFullYear()}</td>
                                    <td>{ hours +':'+ minutes +' '+mid}</td>
                                    <td>{appointment.technician.tech_name}</td>
                                    <td>{appointment.reason}</td>

                                </tr>
                            )
                        }
                    })}
                </tbody>
            </table>
        </div>
    )


}
