import React from "react";
import { useState, useEffect } from "react";

function VehicleModelsList() {
    const [models, setVehicles] = useState([]);

    const fetchModels = async () => {
        const url = 'http://localhost:8100/api/models/'
        const result = await fetch(url);
        const modelsJSON = await result.json();
        setVehicles(modelsJSON.models);
    }

    useEffect(() => {
        fetchModels();
    }, []);

    return (
        <div>
            <h2>Vehicle Models</h2>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models?.map(model => {
                        return (
                            <tr key={model.id}>
                                <td>{model.name}</td>
                                <td>{model.manufacturer.name}</td>
                                <td><img src={model.picture_url} alt={model.name} width="300"/></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )

}
export default VehicleModelsList;