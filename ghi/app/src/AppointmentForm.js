import React from "react";

class AppointmentForm extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            automobiles: [],
            customers: [],
            technicians: [],
            date_time: '',
            reason: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.automobiles;
        delete data.customers;
        delete data.technicians;
        const appUrl = 'http://localhost:8080/appointments/'

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(appUrl, fetchConfig);
        if (response.ok) {

            const cleared = {
                automobile: '',
                customer: '',
                technician: '',
                date_time: '',
                reason: '',
            };
            this.setState(cleared);
        }
    }


    handleChange(event) {
        const value = event.target.value;
        const name = event.target.name;
        this.setState({[name]:value});
    }

    async componentDidMount() {
        const autosUrl = 'http://localhost:8100/api/automobiles/';
        const customerUrl = 'http://localhost:8100/api/customers/';
        const techUrl = 'http://localhost:8080/technicians/';
        
        const response = await fetch(autosUrl);
            
        if (response.ok) {
            const data = await response.json();
            this.setState({automobiles: data.autos});
        }
        
        const customerResponse = await fetch(customerUrl);

        if (response.ok) {
            const data = await customerResponse.json();
            this.setState({customers: data.customers});
        }

        const technicianResponse = await fetch(techUrl);

        if (response.ok) {
            const data = await technicianResponse.json();
            this.setState({technicians: data.techs});
        }
        
    }
    
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1 className="text-center">Make an Appointment</h1>
                        <form onSubmit={this.handleSubmit} id="create-technician-form">
                            <div className="mb-3">
                                <select onChange={this.handleChange} required id="automobile_id" name="automobile_id" className="form-select" value={this.state.automobile}>
                                    <option value="">Choose an automobile</option>
                                    {Array.from(this.state.automobiles).map(automobile => {
                                        return (
                                        <option key={automobile.href} value={automobile.id}>
                                            {automobile.vin}
                                        </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChange} required id="customer_id" name="customer_id" className="form-select" value={this.state.customer}>
                                    <option value="">Choose a customer</option>
                                    {Array.from(this.state.customers).map(customer => {
                                        return (
                                        <option key={customer.href} value={customer.id}>
                                            {customer.name}
                                        </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleChange} required id="technician_id" name="technician" className="form-select" value={this.state.technician}>
                                    <option value="">Choose a technician</option>
                                    {Array.from(this.state.technicians).map(technician => {
                                        return (
                                        <option key={technician.href} value={technician.id}>
                                            {technician.tech_name}
                                        </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.date_time} placeholder="Date_Time" required type="text" name="date_time" id="id" className="form-control" />
                                <label htmlFor="date_time">Date and Time (YYYY-MM-DD HH:MM)</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleChange} value={this.state.reason} placeholder="Reason" required type="text" name="reason" id="id" className="form-control" />
                                <label htmlFor="reason">Reason</label>
                            </div>

                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default AppointmentForm;