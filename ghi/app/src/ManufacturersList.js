import React from 'react';
import { useState, useEffect } from 'react';
function ManufacturersList() {
    const [manufacturers, setManufacturers] = useState([]);
    
    const fetchManufacturers = async () => {
        const url = 'http://localhost:8100/api/manufacturers/'
        const result = await fetch(url);
        const manufacturersJSON = await result.json();
        setManufacturers(manufacturersJSON.manufacturers);
    }

    useEffect(() => {
        fetchManufacturers();
    }, [])


    return (
        <div>
            <h2>Manufacturers</h2>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers?.map(manufacturer => {
                        return (
                            <tr key={ manufacturer.id }>
                                <td>{ manufacturer.name }</td>
                                
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default ManufacturersList;