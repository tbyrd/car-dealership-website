from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Technician, AutomobileVO, CustomerVO, Appointment
from .encoders import TechListEncoder, AutomobileVOEncoder, CustomerVOListEncoder, AppointmentEncoder

# Create your views here.
@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        techs = Technician.objects.all()
        return JsonResponse(
            {"techs": techs},
            encoder=TechListEncoder,
        )
    else:
        content = json.loads(request.body)
        tech = Technician.objects.create(**content)
        return JsonResponse(
            tech,
            encoder=TechListEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_technician(request, id):
    if request.method == "GET":
        tech = Technician.objects.get(id=id)
        return JsonResponse(
            tech,
            encoder=TechListEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "automobile_vin" in content:
                automobile = AutomobileVO.objects.get(import_vin=content["automobile_vin"])
                content["automobile_vin"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile"},
                status=400,
            )
        Technician.objects.filter(id=id).update(**content)
        tech = Technician.objects.get(id=id)
        return JsonResponse(
            tech,
            encoder=TechListEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            automobile = AutomobileVO.objects.get(id=content["automobile_id"])
            content["automobile_id"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile id"},
                status=400,
            )
        try:
            content = json.loads(request.body)
            customer = CustomerVO.objects.get(id=content["customer_id"])
            content["customer_id"] = customer
        except CustomerVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=400,
            )
        try:
            content = json.loads(request.body)
            tech = Technician.objects.get(id=content["technician"])
            content["technician"] = tech
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician id"},
                status=400,
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_appointment(request, id):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "appointments" in content:
                appointment = Appointment.objects.get(id=content["appointment"])
                content["appointment"] = appointment
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid appointment"},
                status=400,
            )
        Appointment.objects.filter(id=id).update(**content)
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )

