from common.json import ModelEncoder

from .models import Technician, AutomobileVO, CustomerVO, Appointment

class TechListEncoder(ModelEncoder):
    model = Technician
    properties = ["id", "tech_name", "employee_number"]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["id", "import_vin"]

class CustomerVOListEncoder(ModelEncoder):
    model = CustomerVO
    properties = ["id", "customer_name"]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = ["id", "automobile", "customer", "technician", "date_time", "reason", "finished",]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "customer": CustomerVOListEncoder(),
        "technician": TechListEncoder(),
    }
