from django.db import models
from django.urls import reverse

# Create your models here.

class Technician(models.Model):
    tech_name = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=20)
    def __str__(self):
        return self.tech_name

class AutomobileVO(models.Model):
    import_vin = models.CharField(max_length=17, unique=True)

class CustomerVO(models.Model):
    customer_name = models.CharField(max_length=100)

class Appointment(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="autos_id",
        on_delete=models.PROTECT,
    )

    customer = models.ForeignKey(
        CustomerVO,
        related_name="customer_id",
        on_delete=models.PROTECT,
    )

    date_time = models.DateTimeField()

    technician = models.ForeignKey(
        Technician,
        related_name="tech_id",
        on_delete=models.PROTECT,
    )

    reason = models.CharField(max_length=200)

    finished = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"pk": self.pk})
    
    def __str__(self):
        return self.id