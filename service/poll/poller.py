import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here.
# from service_rest.models import Something
from service_rest.models import AutomobileVO, CustomerVO
def poll():
    while True:
        print('Service poller polling for data')
        try:
            # Write your polling logic, here
            print('testing')
            automobiles_url = "http://inventory-api:8000/api/automobiles"
            response = requests.get(automobiles_url)
            content = json.loads(response.content)
            print("Automobile: ", response.content)
            for automobile in content["autos"]:
                AutomobileVO.objects.update_or_create(
                    import_vin=automobile["vin"],
                    defaults={"import_vin": automobile["vin"]}
                )
                
            customer_url = "http://inventory-api:8000/api/customers"
            response = requests.get(customer_url)
            content = json.loads(response.content)
            print("Customer: ", response.content)
            for customer in content["customers"]:
                CustomerVO.objects.update_or_create(
                    customer_name=customer["name"],
                    defaults={"customer_name": customer["name"]}
                )
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(10)


if __name__ == "__main__":
    poll()
