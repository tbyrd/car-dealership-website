from django.contrib import admin
from .models import SalesPerson, SalesRecord, AutomobileVO, CustomerVO


@admin.register(SalesPerson)
class SalesPersonAdmin(admin.ModelAdmin):
    pass


@admin.register(SalesRecord)
class SalesRecordAdmin(admin.ModelAdmin):
    pass


@admin.register(AutomobileVO)
class AutomobileVOAdmin(admin.ModelAdmin):
    pass

@admin.register(CustomerVO)
class CustomerVOAdmin(admin.ModelAdmin):
    pass