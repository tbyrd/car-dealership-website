from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import SalesPerson, CustomerVO, AutomobileVO, SalesRecord
import json
from .encoders import AutomobileVOEncoder, EmployeeListEncoder, EmployeeDetailEncoder, CustomerEncoder, SalesListEncoder, SaleDetailEncoder

@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = SalesRecord.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesListEncoder,
            safe=False,
        )
    else:  
        content = json.loads(request.body) 
    try:
        automobile = AutomobileVO.objects.get(id=content["automobile"]) 
        content["automobile"] = automobile 
    except AutomobileVO.DoesNotExist:  
        return JsonResponse({"message": "The Automobile Does Not Exist"}, status=400,) 
    try:
        sales_person = SalesPerson.objects.get(id=content["sales_person"]) 
        content["sales_person"] = sales_person 
    except SalesPerson.DoesNotExist:  
        return JsonResponse({"message": "The Sales Person Does Not Exist"},status=400,) 
    try:
        customer = CustomerVO.objects.get(id=content["customer"]) 
        content["customer"] = customer 
    except CustomerVO.DoesNotExist:  
        return JsonResponse({"message": "The Customer Does Not Exist"},status=400,) 
    
    sale = SalesRecord.objects.create(**content) 
    return JsonResponse(
        sale,
        encoder=SaleDetailEncoder,
        safe=False,
    )

@require_http_methods(["DELETE", "GET"])
def api_show_sale(request, id):
     if request.method == "GET": 
         sale = SalesRecord.objects.get(id=id) 
         return JsonResponse( 
            sale, 
            encoder=SaleDetailEncoder,
            safe=False,
        )
     elif request.method == "DELETE": 
         count, _ = SalesRecord.objects.filter(id=id).delete() 
         return JsonResponse({"deleted": count > 0}) 

@require_http_methods(["GET", "POST"])
def api_list_employees(request):
    if request.method == "GET":
        employees = SalesPerson.objects.all()
        return JsonResponse(
            {"employees": employees},
            encoder=EmployeeListEncoder,
            safe=False,
        )
    else:  
        try:
            content = json.loads(request.body) 
            employee = SalesPerson.objects.create(**content)  
            return JsonResponse(
            employee,
            encoder=EmployeeDetailEncoder,
            safe=False,
            )
        except:  
            return JsonResponse({"message": "Could Not Add the Sales Person"},status=400,) 

@require_http_methods(["DELETE", "GET"])
def api_show_employee(request, id):
    if request.method == "GET": 
        employee = SalesPerson.objects.get(id=id) 
        return JsonResponse( 
            employee, 
            encoder=EmployeeDetailEncoder,
            safe=False,
        )
    if request.method == "DELETE": 
        count, _ = SalesPerson.objects.filter(id=id).delete() 
        return JsonResponse({"deleted": count > 0}) 