from django.db import models
from django.urls import reverse

# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True) 

class CustomerVO(models.Model):
    name = models.CharField(max_length=100)
    address= models.CharField(max_length=250)
    phone_number= models.CharField(max_length=14)

    def __str__(self):
        return self.name


class SalesPerson(models.Model):
    name = models.CharField(max_length=200) 
    employee_number = models.PositiveIntegerField()

    def __str__(self):
        return self.name

class SalesRecord(models.Model):
    sales_price = models.PositiveIntegerField()
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobiles",
        on_delete=models.PROTECT,
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="employees",
        on_delete=models.PROTECT,
    )
    customer = models.ForeignKey(
        CustomerVO,
        related_name="customers",
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_show_sale", kwargs={"id": self.id})

    