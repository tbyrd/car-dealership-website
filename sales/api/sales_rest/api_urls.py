from django.urls import path
from .views import api_list_sales, api_list_employees, api_show_employee, api_show_sale

urlpatterns = [
    path("sales/", api_list_sales, name="api_list_sales"),
    path("sales/<int:id>/", api_show_sale, name="api_show_sale"),
    path("employees/", api_list_employees, name="api_list_employees"),
    path("employees/<int:id>/", api_show_employee, name="api_show_employee")
]

