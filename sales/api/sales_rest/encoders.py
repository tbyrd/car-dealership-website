from common.json import ModelEncoder
from .models import AutomobileVO, CustomerVO, SalesPerson, SalesRecord

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["id", "vin"]

class EmployeeListEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_number", "id"]

class EmployeeDetailEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_number"]

class CustomerEncoder(ModelEncoder):
    model = CustomerVO
    properties = ["name", "address", "phone_number", "id"]

class SalesListEncoder(ModelEncoder):
    model = SalesRecord
    properties = ["sales_price", "automobile", "sales_person", "customer", "id"]
    encoders = { 
        "sales_person": EmployeeListEncoder(),
        "automobile": AutomobileVOEncoder(),
        "employee": EmployeeDetailEncoder(),
        "customer": CustomerEncoder(),
    }

class SaleDetailEncoder(ModelEncoder):
    model = SalesRecord
    properties = ["sales_price", "automobile", "sales_person", "customer", "id"]
    encoders = { 
        "automobile": AutomobileVOEncoder(),
        "sales_person": EmployeeDetailEncoder(),
        "customer": CustomerEncoder(),
    }
    def get_extra_data(self, o):
        return super().get_extra_data(o)