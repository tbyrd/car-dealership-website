import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

from sales_rest.models import AutomobileVO
from sales_rest.models import CustomerVO

def poll():
    while True:
        print('Sales poller polling for data')
        try:
            auto_url = "http://inventory-api:8000/api/automobiles/"
            response = requests.get(auto_url)
            content = json.loads(response.content)
            print("Content", content)
            for automobile in content["autos"]:
                AutomobileVO.objects.update_or_create(
                vin=automobile["vin"],
                defaults={"vin": automobile["vin"]},
            ) 
            url = "http://inventory-api:8000/api/customers/"
            response = requests.get(url)
            content = json.loads(response.content)
            print("Content", content)
            for customer in content["customers"]:
                CustomerVO.objects.update_or_create(
                name=customer["name"],
                defaults={"name": customer["name"]},
            ) 
        
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)

if __name__ == "__main__":
    poll()
